import Vue from 'vue';
import './style.css';


new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue'
     }
});


var app2 = new Vue({
    el: '#app-2',
    data: {
        msg: 'Simple Component'
     }
});

var app5 = new Vue({
	  el: '#app-5',
	  data: {
	    message: 'AEM Vue Integration!'
	  },
	  methods: {
	    reverseMessage: function () {
	      this.message = this.message.split('').reverse().join('')
	    }
	  }
	});

var app6 = new Vue({
	  el: '#app-6',
	  data: {
	    message: 'I m textbox !'
	  }
	});