const path = require('path');
var webpack = require("webpack");

module.exports = [
		{
			name : "index",
			entry : {
				"index" : "./src/index.js",
				"index.min" : "./src/index.js"
			},
			output : {
				path : path.join(__dirname, "clientlibs/js"),
				filename : "[name].js"
			},
			resolve : {
				alias : {
					vue : 'vue/dist/vue.js'
				}
			},
			module : {
				rules : [ {
					test : /\.css$/,
					use : [ 'style-loader', 'css-loader' ]
				}, {
					test : /\.(png|svg|jpg|gif)$/,
					use : [ 'file-loader' ]
				}, {
					test : /\.(woff|woff2|eot|ttf|otf)$/,
					use : [ 'file-loader' ]
				} ]

			},
			plugins : [ new webpack.optimize.UglifyJsPlugin({
				include : /\.min\.js$/,
				minimize : true
			}) ]
		},
		{
			name : "bundle",
			entry : {
				"bundle" : "./src/index.js",
				"bundle.min" : "./src/index.js"
			},
			devtool : "eval",
			output : {
				path : path.join(__dirname,
						"../components/content/vuecomponent/clientlibs/js"),
				filename : "[name].js"
			},
			resolve : {
				alias : {
					vue : 'vue/dist/vue.js'
				}
			},
			module : {
				rules : [ {
					test : /\.css$/,
					use : [ 'style-loader', 'css-loader' ]
				}, {
					test : /\.(woff|woff2|eot|ttf|otf)$/,
					use : [ 'file-loader' ]
				} ]
			},
			plugins : [ new webpack.optimize.UglifyJsPlugin({
				include : /\.min\.js$/,
				minimize : true
			}) ]
		}

];
